#Changelog

## [Unreleased]
- suppression et modification de tag
- rangeslider dans la fenetre d'affichage des timelines
- affichage sur une meme ligne des timelines de tags exlcusifs

## [0.1.0] - 2019-05-31
### Added
- gestion chevauchement des timelines
- utilisation scss
### Changed
- gestion des feuilles de styles
### Removed

### Fixed
- Bug affichage width timeline

## [0.1.0] - 2019-05-24
### Added
- rangeslider dans controlbar de videojs
- modification de timelines
### Changed
- gestion timelines
### Removed
### Fixed
- Affichage durée enregistrement tag
- 

## [0.1.0] - 2019-03-17
### Added
- ajout du composant videojs
- ajout de composant tags
- ajout plugins videojs

## [0.1.0] - 2019-05-03
### Added
- Formulaire de saisie de nouveau tag
- Ajout de fenêtre timeline (non integrée à videojs)
### Changed
- Tags: affichage des nouveaus tags
### Removed
### Fixed
- Calcul de la durée d'un timeline

## [0.1.0] - 2019-03-17
### Added
- ajout du composant videojs
- ajout de composant tags
- ajout plugins videojs

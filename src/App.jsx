import { h, render } from 'preact'
import VideoPlayer from './components/VideoPlayer'
//import EventLogger from './components/VjsEvents'
import {ControlBoard,ManageTagsBoard,VideoTimeline} from './components/ControlButton'
import VideoApp from './RenderPlayer'
import ExamplePlugin from './components/VjsPlugins'




const idPlayer = `video-player-${(new Date) * 1}`;
const videoJsOptions = {
    autoplay: false,
    controls: true,
    id: idPlayer,
    src:"./assets/oceans.mp4",
    poster:"./assets/logo-preact.png",
    width:500,
    height:250,
    plugins:{}
};
/*
const element = <VideoApp options={videoJsOptions}/>;

const tagsAvailableArray = [];
tagsAvailableArray.push({index:1, label:"D'accord", level:1, isExclusive:true});
tagsAvailableArray.push({index:2, label:"Pas d'accord", level:1, isExclusive:true});
tagsAvailableArray.push({index:3, label:"Speaker 1", level:0, isExclusive:true});
tagsAvailableArray.push({index:4, label:"Speaker 2", level:0, isExclusive:true});
tagsAvailableArray.push({index:5, label:"Speaker 3", level:0, isExclusive:true});
*/
///render(<ControlBoard tagsAvailable={tagsAvailableArray} idPlayer={idPlayer}/>, document.getElementById('ControlsContainer'));
///render(<ManageTagsBoard tagsAvailable={tagsAvailableArray}/>, document.getElementById('ManageTagContainer'));
render(<VideoTimeline videoptions={videoJsOptions} idplayer={idPlayer}/>, document.getElementById('PlayerTimelineContainer'));
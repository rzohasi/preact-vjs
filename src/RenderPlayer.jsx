import {h, Component } from 'preact';
import VideoPlayer from './components/VideoPlayer';

class VideoApp extends Component {
    constructor(props){
        super(props);
        this.player = {}
//x        this.state.playerDuration = 0;
    }
    componentDidMount() {
        this._mounted = true;
    }

    onPlayerReady(player){
///        console.log("Player is ready: ", player);
        this.player = player;

    }

    onVjsPlay(duration){
//        console.log("Video played at: ", duration);
        document.getElementById('timelinedisplay').style.display = "none";
//        this.player.getChild('VideojsTimelineContainer').show();
    }

    onVjsTimeUpdate(duration){
///        console.log("Time updated: ", duration);
    }

    onVjsPause(duration){
///        console.log("Video paused at: ", duration);
    }

    onVjsSeeking(duration){
///        console.log("Video seeking: ", duration);
    }

    onVjsSeeked(from, to){
///        console.log(`Video seeked from ${from} to ${to}`);
    }

    onVjsEnd(){
///        console.log("Video ended");
    }

    render() {
        return (
            <div>
                <VideoPlayer
                    controls={true}
                    idPlayer={this.props.options.id}
                    src={this.props.options.src}
                    poster={this.props.options.poster}
                    width={this.props.options.width}
                    height={this.props.options.height}
                    onReady={this.onPlayerReady.bind(this)}
                    onPlay={this.onVjsPlay.bind(this)}
                    onPause={this.onVjsPause.bind(this)}
                    onTimeUpdate={this.onVjsTimeUpdate.bind(this)}
                    onSeeking={this.onVjsSeeking.bind(this)}
                    onSeeked={this.onVjsSeeked.bind(this)}
                    onEnd={this.onVjsEnd.bind(this)}
                    ref={videoRef => this.videoRef = videoRef}
                    handleChangeDivSize={this.props.handleChangeDivSize}
                />
            </div>
        );
    }
}
export default VideoApp;
import {h, Component, render} from 'preact';
import videojs from 'video.js';
import * as moment from 'moment';
import 'moment-duration-format';
import VideoApp from "../RenderPlayer";
import styles from '../styles/styles.scss';

/**
 * recording icon display
 */
class DisplayRec extends Component{
    constructor(props){
        super(props);
        this.state={
            inRecord:false
        }
    }
    componentDidMount() {
        this.player = videojs(this.props.idPlayer);
        this.player.on('play', () => {
            if(this.props.inRec){
                this.setState({inRecord:this.props.inRec})
            }
        });
        //disable button, pause rec
        this.player.on('pause', () => {
            if(this.props.inRec){
                this.setState({inRecord:false})
            }
        });
        this.player.on('seeking', () => {
            this.player.off('timeupdate', () => { });
            this.player.one('seeked', () => { });
//            this.props.onSeeking(this.player.currentTime());
        });
    }
    componentWillReceiveProps(nextProps) {
        this.setState({inRecord:nextProps.inRec})
    }

    render(){
        //let currentIntervalTime = !previousStep2 ?0 : moment.duration(currentEndTime - currentBeginTime, "minutes").format();
        let classNameRec = 'recStyle';
        if(this.props.inRec){
            classNameRec += ' recActive';
        }
        return (
            <div className={classNameRec}>
                <div className="recIcon"></div>
                <TimerRec start={Date.now()} previousTotalTime={this.props.previousTotalTime} inRec={this.state.inRecord} idPlayer={this.props.idPlayer}/>
            </div>
        );
    }
}

/**
 * tag button display
 */
class TagPane extends Component{
    constructor(props){
        super(props);
    }
    handleClick(e){
        this.props.handleClick(e.target.value); //in ControlBoard
    }
    render(){
        let classNameTag = `button-c tagcolor-${this.props.idx}`;
        return(
            <div class="tag-row">
                <button
                    className={classNameTag}
                    onClick={(e) => this.handleClick(e)}
                    value={this.props.value}
                    level={this.props.level}
                    disabled={ this.props.isButtonDisabled }
                >
                    {this.props.label}
                </button>
                <DisplayRec inRec={this.props.tagStatus.inRecord} previousTotalTime={this.props.tagStatus.totalTime} idPlayer={this.props.idPlayer}/>
            </div>
        );
    }
}

/**
 *  Panel for tags display
 */
export class ControlBoard extends Component{
    constructor(props){
        super(props);
        this.listButton = this.getAllButtons("button-c")
        this.state = {
            stepStatus:this.props.stepStatus
        }
        this.handleClick = this.handleClick.bind(this);
    }
    getAllButtons(className){
        return document.getElementsByClassName(className);
    }

    /**
     * handle click for TagPane
     * @param i
     */
    handleClick(i){
        this.props.onRecordTimeline(i); //handleClickRecordTimeline in VideoTimeline

        let currentValue = i;
        let currentLevel = this.props.tagsAvailable[i].level;
        if(this.props.tagsAvailable[i].isExclusive && !this.props.stepStatus[i].inRecord){
            for (let item of this.listButton) {
                if (item.value != currentValue && item.getAttribute('level') == currentLevel){
                    item.disabled = 'disabled'
                }
            }
        }
        else{
            for (let item of this.listButton) {
                if (item.value != currentValue && item.getAttribute('level') == currentLevel)
                    item.disabled = ''
            }
        }
        //show comment form
    }
    componentDidMount() {
        this.player = videojs(this.props.idPlayer);
        this.player.on('play', () => {
            for (let item of this.listButton) {
                item.disabled = ''
            }
        });
        //disable button, pause rec
        this.player.on('pause', () => {
            for (let item of this.listButton) {
                item.disabled = 'disabled'
            }
        });
        this.player.on('ended', () => {
            Object.keys(this.props.tagsAvailable).map(key =>{
                if(this.props.stepStatus[key].inRecord){
                    this.props.onRecordTimeline(key);
                }
            });
        });
    }
    render(){
        this.tagsAvailable = Object.keys(this.props.tagsAvailable).map(key =>
            <TagPane
                label={this.props.tagsAvailable[key].label}
                value={key}
                idx={this.props.tagsAvailable[key].index}
                level={this.props.tagsAvailable[key].level}
                isExclusive={this.props.tagsAvailable[key].isExclusive}
                handleClick={i => this.handleClick(key)}
                isButtonDisabled={true}
                ref={tagpane => this.tagpane = tagpane}
                tagStatus={this.props.stepStatus[key]}
                timeRecord={0}
                idPlayer={this.props.idPlayer}
            />);
        return(
            <div className="tag-board">
                <div className="control-board">
                    {this.tagsAvailable}
                </div>
            </div>
        )
    }
}

/**
 * Display total of recording time
 */
class TimerRec extends Component{
    constructor(props){
        super(props);
        this.state = {
            timeElapsedSecond : 0
        }
    }
    componentDidMount() {
            this.interval = setInterval(() => this.tick(), 1000);
    }
    componentWillUnmount() {
        clearInterval(this.interval);
    }
    tick() {
        if(this.props.inRec){
            this.setState({timeElapsedSecond: this.state.timeElapsedSecond + 1});
        }
    }
    toHHMMSS(seconds){
        var sec_num = parseInt(seconds, 10); // don't forget the second param
        var hours   = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);

        if (hours   < 10) {hours   = "0"+hours;}
        if (minutes < 10) {minutes = "0"+minutes;}
        if (seconds < 10) {seconds = "0"+seconds;}
        return hours+':'+minutes+':'+seconds;
    }
    render() {
        let secondsElapsed = this.toHHMMSS(this.state.timeElapsedSecond);
        return (
            <span className="recTimer">{secondsElapsed}</span>
        )
    }
}

/**
 * Tag management component
 */
class ManageTagPane extends Component{
    constructor(props){
        super(props);
        this.state = {isToggleOn: false};
        this.onSubmit = this.onSubmit.bind(this);

    }
    componentDidMount() {
        this.taglabel.focus();
    }
    onSubmit(event) {
        event.preventDefault();

        let newTagValue = {
            index: this.props.tagsAvailable.length + 1,
            label: this.taglabel.value,
            level: Math.trunc(this.tagLevel.value),
            isExclusive: this.tagIsExclusive.checked
        };
/*
        newTagValue["label"] = this.taglabel.value;
        newTagValue["level"] = Math.trunc(this.tagLevel.value);
        newTagValue["isExclusive"] = this.tagIsExclusive.checked;
*/
//        if(newTagValue) {
//            this.props.addTag(newTagValue);
            this.tagform.reset();
//        }
        this.props.onSaveTag(newTagValue);
    }
    render(){
        return(
            <div className="tag-config">
                <form ref={tagform => this.tagform = tagform} onSubmit={this.onSubmit} className="form-inline">
                    <label>
                        <span>Libellé du tag:</span>
                    <input
                        type="text"
                        ref={taglabel => this.taglabel = taglabel}
                        className="form-control"
                        placeholder="Saisir nom du tag"/>
                    </label>
                    <label>
                        <span>Exclusive:</span>
                        <input
                            name="isExclusive"
                            ref={tagIsExclusive => this.tagIsExclusive = tagIsExclusive}
                            type="checkbox"
                            value="1"/>
                    </label>
                    <label>
                        <span>Niveau:</span>
                    <input
                        type="text"
                        ref={tagLevel => this.tagLevel = tagLevel}
                        className="form-control"
                        placeholder="Saisir le niveau"/>
                    </label>
                    <button type="submit" className="btn btn-default">Enregistrer</button>
                </form>
            </div>
        );
    }
}

/**
 * Panel of tag management
 */
export class ManageTagsBoard extends Component{
    constructor(props){
        super(props);
        this.handleClick = this.handleClick.bind(this);
        this.handleClickClose = this.handleClickClose.bind(this);
        this.state = {
            showManagePane: false,
            tagsAvailable: this.props.tagsAvailable,
        };
    }
    handleClick(){
        this.setState({showManagePane: true});
    }
    handleClickClose(){
        this.setState({showManagePane: false});
    }
    render(){
        let manageTagPanel;
        let closeButton;
        let openButton;
        const showPane = this.state.showManagePane
        if(showPane){
            manageTagPanel = <ManageTagPane
                addTag={this.addTag}
                onSaveTag={this.props.onSaveTag}
                tagsAvailable={this.props.tagsAvailable}
            />
            closeButton = <button onClick={this.handleClickClose}>Fermer</button>
        }
        else{
            openButton  = <button onClick={this.handleClick}>+ Ajouter un tag</button>
        }
        return (
            <div className="tag-manage">
                <div className="tag-add">
                    {openButton}
                    {closeButton}
                </div>
                {manageTagPanel}
            </div>
        );
    }
}

/**
 * Main component
 */
export class VideoTimeline extends Component{
    constructor(props){
        super(props);
        //tags list
        const tagsAvailable = [];

        tagsAvailable.push({index:1, label:"D'accord", level:1, isExclusive:true});
        tagsAvailable.push({index:2, label:"Pas d'accord", level:1, isExclusive:true});
        tagsAvailable.push({index:8, label:"Oui mais", level:1, isExclusive:true});
        tagsAvailable.push({index:3, label:"Speaker 1", level:0, isExclusive:false});
        tagsAvailable.push({index:4, label:"Speaker 2", level:0, isExclusive:false});
        tagsAvailable.push({index:5, label:"Speaker 3", level:0, isExclusive:false});
        tagsAvailable.push({index:6, label:"Période 1", level:2, isExclusive:true});
        tagsAvailable.push({index:7, label:"Période 2", level:2, isExclusive:true});
        tagsAvailable.push({index:9, label:"1 Non Exclu level 3", level:3, isExclusive:false});
        tagsAvailable.push({index:10, label:"2 Non Exclu level 3", level:3, isExclusive:false});

        //init timeline
        let initialStep = [];
        Object.keys(tagsAvailable).map(key => {
            initialStep.push({
                indexTag: tagsAvailable[key].index,
                inRecord:false,
                beginTime:0,
                endTime: 0,
                intervalTime : 0,
                totalTime: 0,
                levelTag: tagsAvailable[key].level,
                isExclusive : tagsAvailable[key].isExclusive
            });
        }),

        this.state = {
            tagsAvailableArray: tagsAvailable,
            stepStatus: initialStep,
            historyTimeline:[],
            resizeTimeline:false,
            showCommentForm:false,
            showButtonDelete:false,
            idHistorySelected:0
        };
        //bind
        this.handleSaveTag = this.handleSaveTag.bind(this);
        this.handleClickRecordTimeline = this.handleClickRecordTimeline.bind(this);
        this.handleClickSpan = this.handleClickSpan.bind(this);
        this.handleTimelineResize = this.handleTimelineResize.bind(this);
        this.handleSaveComment = this.handleSaveComment.bind(this);
        this.handleClickDelTimeline = this.handleClickDelTimeline.bind(this);
    }

    /**
     * saving new tag
     * @param newtag
     */
    handleSaveTag(newtag){
        this.setState(prevState => {
            const prevTagsAvailableArray = [...prevState.tagsAvailableArray ];
            prevTagsAvailableArray.push(newtag);
            return { tagsAvailableArray: prevTagsAvailableArray };
        });
        localStorage.setItem("tagsAvailableArray", JSON.stringify(this.state.tagsAvailableArray));
        this.setState(prevState => {
            const prevStepStatus = [...prevState.stepStatus ];
            let nextIndexTag = Math.max.apply(Math, prevStepStatus.map(function(o) { return o.indexTag; }));
            prevStepStatus.push({
                indexTag: newtag.index,
                inRecord:false,
                beginTime:0,
                endTime: 0,
                intervalTime : 0,
                totalTime: 0,
                levelTag : newtag.level,
                isExclusive: newtag.isExclusive
            });
            return { stepStatus: prevStepStatus };
        });
        localStorage.setItem("stepStatus", JSON.stringify(this.state.stepStatus));
    }

    /**
     * used as prop in ControlBoard
     * @param numtag
     */
    handleClickRecordTimeline(numtag){
        //show RSTimeBar
        let progressControl = this.player.getChild('ControlBar').getChild('ProgressControl').getChild('SeekBar');
        progressControl.getChild('RSTimeBar').hide();

        let whereTime = this.player.currentTime() * 1000;
        let _showCommentForm = true;
        this.setState(prevState => {
            const stepIsStarting = [...prevState.stepStatus];
            let currentStep = stepIsStarting[numtag];
            let previousStep = currentStep.inRecord;
            let previousTotalTime = currentStep.totalTime;
            let currentBeginTime = previousStep===false? whereTime: currentStep.beginTime;
            let currentEndTime = previousStep===true? whereTime: currentStep.endTime;
            let currentIntervalTime = !previousStep ?0 : currentEndTime - currentBeginTime;
            let currentIdTag = currentStep.indexTag;
            let totalTime = previousTotalTime + currentIntervalTime;
            let currentTagLevel = currentStep.levelTag;
            let isExclusive = currentStep.isExclusive;
            stepIsStarting[numtag] = {
                indexTag: currentIdTag,
                inRecord:!previousStep,
                beginTime : currentBeginTime,
                endTime : currentEndTime,
                intervalTime : currentIntervalTime,
                totalTime : totalTime,
                levelTag: currentTagLevel,
                isExclusive: isExclusive
            };
            let prevHistory = [...prevState.historyTimeline];
            if(previousStep && totalTime > 0){
                //check adjacent, contains, overlaps, ...
                prevHistory = this.resizeHistoryTimeline(prevHistory,currentIdTag, currentBeginTime, currentEndTime, currentTagLevel, isExclusive);
                //close comment form
                _showCommentForm = false;
            }
            localStorage.setItem("historyTimeline", JSON.stringify(prevHistory));
            return { stepStatus: stepIsStarting, historyTimeline: prevHistory, showCommentForm:_showCommentForm, resizeTimeline:true};
        });
    }

    /**
     * used to check new range position (adjacent, contains, within, overlaps
     * @param _prevHistory
     * @param currentIdTag
     * @param currentBeginTime
     * @param currentEndTime
     * @returns {*[]}
     */
    resizeHistoryTimeline(_prevHistory,currentIdTag, currentBeginTime, currentEndTime, currentTagLevel, isExclusiveTL){
        let updateHistory = true;
        let elemToDelete = [];
        //check all timeline of same tag
        _prevHistory.forEach((timeline,index)=>{
            let beginTL = timeline.begin;
            let endTL = timeline.end;
            let idtagTL = timeline.idtag;
            let levelTL = timeline.level;
            if(currentIdTag === idtagTL || levelTL === currentTagLevel){
                let endIsOverriding = currentEndTime.between(beginTL, endTL, true);
                let beginIsOverriding = currentBeginTime.between(beginTL, endTL, true);

                let endTlIsIncluded = endTL.between(currentBeginTime, currentEndTime, true);
                let beginTlIsIncluded = beginTL.between(currentBeginTime, currentEndTime, true);

                if(endIsOverriding && beginIsOverriding){
                    //timeline within
                    updateHistory = false;
                }
                else{
                    if(endTlIsIncluded && beginTlIsIncluded){
                        //timeline overlap
                        elemToDelete.push(index.toString());
                    }
                    else if(endIsOverriding){
                        //adjacent, left
                        elemToDelete.push(index.toString());
                        currentEndTime = endTL;
                    }
                    else if(beginIsOverriding){
                        //adjacent, right
                        elemToDelete.push(index.toString());
                        currentBeginTime = beginTL;
                    }
                }
            }

        });
        if(updateHistory){
            //let nextindex = _prevHistory.length;
            let nextindex = _prevHistory.length===0 ? 1 : Math.max.apply(Math, _prevHistory.map(function(obj) { return obj.idHistory; }));
            _prevHistory.push({
                "idtag":Math.trunc(currentIdTag),
                "level": currentTagLevel,
                "begin":currentBeginTime,
                "end":currentEndTime,
                "levelTag": currentTagLevel,
                "isExclusive":isExclusiveTL,
                "idHistory": nextindex + 1
            });
        }
        const filtered = Object.keys(_prevHistory)
            .filter(key => !elemToDelete.includes(key))
            .reduce((obj, key) => {
                obj[key] = _prevHistory[key];
                return obj;
            }, {});
        if(elemToDelete.length > 0){
            _prevHistory = [];
            _prevHistory = Object.keys(filtered).map(key=> {
                return (filtered[key]);
            });
        }
        return _prevHistory;
    }

    /**
     * called from
     * @param e
     */
    handleClickSpan(e){
        this.player = videojs(this.props.idplayer);

        //show RSTimeBar
        let progressControl = this.player.getChild('ControlBar').getChild('ProgressControl').getChild('SeekBar');
        progressControl.getChild('RSTimeBar').show();

        let _showCommentForm = !this.state.showCommentForm;
        let _showButtonDelete = !this.state.showButtonDelete;

        let idHistory = e.target.getAttribute('idhistory');
        let idDivTimelineClicked = e.target.id;
        let tagHistory = this.state.historyTimeline.find(obj => {
            idHistory = parseInt(idHistory, 10);
            return obj.idHistory === idHistory
        });
        let segmentBegin = tagHistory.begin / 1000;
        let segmentEnd = tagHistory.end / 1000;
        this.rs = videojs.getPlugin('TimelinePlugin');
        this.rs['idDivTimelineClicked'] = idDivTimelineClicked;
        this.rs['idHistoryTimelineClicked'] = idHistory;
        this.player.TimelinePlugin()._reset();
        this.player.TimelinePlugin().setValue(0, segmentBegin);
        this.player.TimelinePlugin().setValue(1, segmentEnd);
        this.player.currentTime(segmentBegin);

        this.setState({
            showCommentForm: !this.state.showCommentForm,
            showButtonDelete: true,
            idHistorySelected: idHistory
        });
    }
    componentDidMount() {
        this.player = videojs(this.props.idplayer);
        this.restoreStateWithLocalStorage();
    }
    refreshTimelineContainer(){
        let playerHeight = parseFloat(this.player.height());
        let elem = document.getElementsByClassName("vjs-control-bar");
        let controlBarHeight = parseFloat(window.getComputedStyle(elem[0]).getPropertyValue("height"));
        let TimelinePanelHeight = playerHeight - controlBarHeight;
    }

    /**
     * call when resizing timeline via rangeslider
     * @param side
     * @param currentTimePlayer
     */
    handleTimelineResize(side, currentTimePlayer){
        let idHistoryTimelineClicked = this.rs['idHistoryTimelineClicked'];

        this.setState(prevState => {
            const previousHistory = [...prevState.historyTimeline];
            let currentHistory = previousHistory.find(obj => {
                idHistoryTimelineClicked = parseInt(idHistoryTimelineClicked, 10);
                return obj.idHistory === idHistoryTimelineClicked
            });

            let currentIdTag = currentHistory.idtag;
            let currentPosition = currentTimePlayer * 1000;
            let currentBeginTime= 0;
            let currentEndTime = 0;
            let currentLevel = currentHistory.level;
            if(side === 0){
                currentHistory.begin = currentPosition;
                currentBeginTime = currentPosition;
                currentEndTime = currentHistory.end;
            }
            else{
                currentHistory.end = currentPosition;
                currentBeginTime = currentHistory.begin;
                currentEndTime = currentPosition;
            }
            let _prevHistory = this.resizeHistoryTimeline(previousHistory,currentIdTag, currentBeginTime, currentEndTime, currentLevel);
            localStorage.setItem("historyTimeline", JSON.stringify(_prevHistory));
            return {historyTimeline: _prevHistory,resizeTimeline:true};
        });

    }
    restoreStateWithLocalStorage(){
        // for all items in state
        for (let key in this.state) {
            // if the key exists in localStorage
            if (localStorage.hasOwnProperty(key)) {
                // get the key's value from localStorage
                let value = localStorage.getItem(key);
                // parse the localStorage string and setState
                try {
                    value = JSON.parse(value);
                    this.setState({ [key]: value });
                } catch (e) {
                    // handle empty string
                    this.setState({ [key]: value });
                }
            }
        }
    }

    handleSaveComment(idHistory, comment){
        this.setState(prevState => {
            const previousHistory = [...prevState.historyTimeline];

//            localStorage.setItem("historyTimeline", JSON.stringify(previousHistory));
//            return {historyTimeline: previousHistory, showCommentForm:false, showButtonDelete:false};
        });
    }
    handleClickDelTimeline(idHistory){
        this.setState(prevState => {
            const previousHistory = [...prevState.historyTimeline];
            let removeIndex = previousHistory.map(function(item) { return item.idHistory; }).indexOf(idHistory);
            ~removeIndex && previousHistory.splice(removeIndex, 1);

            localStorage.setItem("historyTimeline", JSON.stringify(previousHistory));
            return {historyTimeline: previousHistory, showCommentForm:false, showButtonDelete:false};
        }, console.log('-------------------------- callbak'));

    }
    render(){
        let divStyle = {
            display:this.state.resizeTimeline?'':'none'
            //display:''
        };
        const style = {
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            border: "solid 1px #ddd",
            background: "#f0f0f0"
        };
        // Custom CSS for ResizableBox
        let customStyles = {
            marginTop: this.state.marginBetBoxes + 'px',
            backgroundColor: 'transparent'
        };
        return(
            <div className="videotimeline">
                <div>
                    <div className="videojscontainer">
                        <VideoApp
                            options={this.props.videoptions}
                            historyTimeline={this.state.historyTimeline}
                            segment={this.state.segment}
                            handleChangeDivSize={this.handleTimelineResize}
                        />

                    </div>
                    <div className="tagscontainer">
                        <ControlBoard
                            tagsAvailable={this.state.tagsAvailableArray}
                            idPlayer={this.props.idplayer}
                            stepStatus = {this.state.stepStatus}
                            onRecordTimeline = {this.handleClickRecordTimeline}
                            idPlayer={this.props.idplayer}
                        />
                    </div>
                </div>
                <CommentTextarea
                    showCommentForm={this.state.showCommentForm}
                    idHistorySelected={this.state.idHistorySelected}
                    handleSaveComment={this.handleSaveComment}
                />
                <div className="controltagcontainer">
                    <ManageTagsBoard
                        tagsAvailable={this.state.tagsAvailableArray}
                        onSaveTag={this.handleSaveTag}
                    />
                        <ClearLocalStorage/>
                </div>
                <div className="timelinecontainer" id="timelinedisplay" style={divStyle}>
                    <TimelineDisplay
                        historyTimeline={this.state.historyTimeline}
                        idplayer={this.props.idplayer}
                        handleClickSpan = {this.handleClickSpan}
                        tagsAvailable={this.state.tagsAvailableArray}
                        handleClickDelTimeline={this.handleClickDelTimeline}
                    />
                </div>
                <ScrollingDiv></ScrollingDiv>
            </div>
        );
    }
}

/**
 * Container of timelines panel
 */
class TimelineDisplay extends Component{
    constructor(props){
        super(props);
    }
    render(){
        if(document.getElementById(this.props.idplayer) === null){
            return <div></div>
        }
        this.player = videojs(this.props.idplayer);
        let playerEl = document.getElementById(this.props.idplayer);
        let boundingPlayer = playerEl.getBoundingClientRect();
        let playerHeight = parseFloat(boundingPlayer.height);
        let playerWidth = parseFloat(boundingPlayer.width);
        let topPlayer = parseFloat(boundingPlayer.top);
        let leftPlayer = parseFloat(boundingPlayer.left);
        let controlBarEl = document.getElementsByClassName('vjs-control-bar')[0];
        let controlBarElHeight = controlBarEl.offsetHeight===0?30:controlBarEl.offsetHeight;    //todo verif height

        let timelineDivHeight = playerHeight - controlBarElHeight;
        let divStyle = {
                width:playerWidth,
                height:timelineDivHeight,
                top:topPlayer,
                left:leftPlayer
            };
        return(
            <div
                className="timelinediv vjs-timeline-container"
                style={divStyle}
                ref={TimelineContainer => this.TimelineContainer = TimelineContainer}>
                <TimelinesBlock
                    historyTimeline={this.props.historyTimeline}
                    idplayer={this.props.idplayer}
                    handleClickSpan = {this.props.handleClickSpan}
                    tagsAvailable={this.props.tagsAvailable}
                    widthTimelineContainer={playerWidth}
                    handleClickDelTimeline={this.props.handleClickDelTimeline}
                />
            </div>
        );
    }
}

/**
 * Component for displaying all tags
 */
class TimelinesBlock extends Component{
    constructor(props){
        super(props);

        this.player = videojs(this.props.idplayer);
        this.state = {
            allTimelines: this.props.historyTimeline,
            indexActiveHistoryId: 0,
        };
        this.tagTimelines = {};
        this.handleClickSpan = this.handleClickSpan.bind(this)
        this.rowTimelinesLevel = [];
    }
    componentWillMount() {

    }
    handleClickSpan(e){
        this.setState({indexActiveHistoryId: e.target.getAttribute('idhistory')});
        this.props.handleClickSpan(e);
    }
    componentWillReceiveProps(newProps){
        this.setState({allTimelines:newProps.historyTimeline});
        this.refreshTimeline();
    }
    refreshTimeline(){
        let count = 0;
        let allTimelines = this.state.allTimelines;
        const distinctLevel = [...new Set(allTimelines.map(x => x.level))];
        distinctLevel.sort();

        this.tagTimelines = {};
        let tabLevel = [];
        let i = 0;
        this.props.tagsAvailable.map(key => {
            this.tagTimelines[key.index] = []
            if(key.isExclusive){
                tabLevel[key.level] = i;
            }
            else{
                tabLevel[key.level] = i;
                i++;
            }
        });
        //rangée timeline
        let filteredArrayExclusive_ = [];
        for(let key in distinctLevel){
            let filterConditionExclusive = true;
            let currentLevel = distinctLevel[key];
            let filterConditionLevel = parseInt(currentLevel, 10);
            let tempArr = []
            tempArr = allTimelines.filter(el=> {
                return el.isExclusive == filterConditionExclusive && el.level == filterConditionLevel;
            });
            if(tempArr.length > 0){
                filteredArrayExclusive_[currentLevel] = tempArr;
            }
        }
        let filteredArrayNonExclusive_ = [];
        for(let key in distinctLevel){
            let filterConditionExclusive = false;
            let currentLevel = distinctLevel[key];
            let filterConditionLevel = parseInt(currentLevel,10);
            let tempArr = []
            tempArr = allTimelines.filter(el=> {
                return el.isExclusive == filterConditionExclusive && el.level == filterConditionLevel;
            });
            if(tempArr.length > 0){
                filteredArrayNonExclusive_[currentLevel] = tempArr;
            }
        }
        //ligne des timelines exclusive
        let rowTimelinesLevel = [];
        for(let level in filteredArrayExclusive_){
            this.rowTimelinesLevel[level] = this.createRowTimelinesExclusive(filteredArrayExclusive_[level]);
        }
        for(let level in filteredArrayNonExclusive_){
            this.rowTimelinesLevel[level] = this.createRowTimelinesNonExclusive(filteredArrayNonExclusive_[level]);
        }
    }
    createRowTimelinesExclusive(allTimelines){
        let rowTimelines = [];
        let count = 0;
        for (let item in allTimelines) {
            let at = allTimelines[item];
            let idTag = at.idtag;
            let levelTag = at.level;
            let idHistory = at.idHistory;
            let start = this._percent(at.begin/1000) * 100;
            let end = this._percent(at.end/1000) * 100;
            let width = Math.min(100, Math.max(0.2, end - start));
            let divClass = `divtimeline tagcolor-${idTag}`;
            if(start.toFixed(0) == end.toFixed(0)){
                width = '';
                divClass = "divtimeline point"
            }
            let positionTopTag = at.isExclusive;

            let tagObj = this.props.tagsAvailable.find(obj => {
                idTag = parseInt(idTag, 10);
                return obj.index === idTag
            });
            let titleTL = tagObj.label;
            rowTimelines.push(
                <SpanTimeline
                    class={divClass}
                    id={`timeline_${idTag}_${count}`}
                    idHistory={idHistory}
                    key={count}
                    width={width + '%'}
                    top={levelTag+"em"}
                    left={start+'%'}
                    start={at.begin}
                    end={at.end}
                    handleClickSpan = {this.handleClickSpan}
                    titleTL={titleTL}
                    handleClickDelTimeline={this.props.handleClickDelTimeline}
                    timelineIsActive={this.state.indexActiveHistoryId==idHistory?true:false}
                    dashedHeight={this.player.height()}
                />
            );
            count++;
        }
        return rowTimelines;
    }
    createRowTimelinesNonExclusive(allTimelines){
        let rowTimelines = [];
        let count = 0;
        this.props.tagsAvailable.map(key => {
            rowTimelines[key.index] = []
        });
        for (let item in allTimelines) {
            let at = allTimelines[item];
            let lineSpan = at.idtag;
            let idTag = at.idtag;
            let levelTag = at.level;
            let idHistory = at.idHistory;
            let start = this._percent(at.begin/1000) * 100;
            let end = this._percent(at.end/1000) * 100;
            let width = Math.min(100, Math.max(0.2, end - start));
            let divClass = `divtimeline tagcolor-${idTag}`;
            if(start.toFixed(0) == end.toFixed(0)){
                width = '';
                divClass = "divtimeline point"
            }
            let positionTopTag = at.isExclusive;
            let tagObj = this.props.tagsAvailable.find(obj => {
                idTag = parseInt(idTag, 10);
                return obj.index === idTag
            });
            let titleTL = tagObj.label;

            rowTimelines[idTag].push(
                <SpanTimeline
                    class={divClass}
                    id={`timeline_${idTag}_${count}`}
                    idHistory={idHistory}
                    key={count}
                    width={width + '%'}
                    top={levelTag+"em"}
                    left={start+'%'}
                    start={at.begin}
                    end={at.end}
                    handleClickSpan = {this.handleClickSpan}
                    titleTL={titleTL}
                    timelineIsActive={this.state.indexActiveHistoryId==idHistory?true:false}
                    handleClickDelTimeline={this.props.handleClickDelTimeline}
                    dashedHeight={this.player.height()}
                />
            );
            count++;
        }
        return (rowTimelines);
    }
    _percent(d){
        let c = this.player.duration();
        //if(isNaN(c)){return 0}
        if(isNaN(c)){
            if (localStorage.hasOwnProperty('playerDuration')) {
                let value = localStorage.getItem('playerDuration');
                try {
                    c = JSON.parse(value);
                } catch (e) {
                    return 0;
                }
            }
            else return 0;
        }
        return Math.min(1,Math.max(0,d/c))
    }
    _seconds(c){
        let d = this.player.duration();
        if(isNaN(d)){return 0}
        return Math.min(d,Math.max(0,c*d))
    }
    render(){
        this.refreshTimeline();

        let RowsTimelines = this.rowTimelinesLevel.map(key => {
            let row = key;
            let rowNonExclusive = []
            for(let elem in key){
                if(Array.isArray(key[elem])){
                    if(key[elem].length > 0){
                        rowNonExclusive.push(<RowTimelinesContainer width={this.props.widthTimelineContainer} children={key[elem]}>
                        </RowTimelinesContainer>);
                    }
                }
            }
            if(rowNonExclusive.length > 0){
                return rowNonExclusive;
            }
            else
                return(
                <RowTimelinesContainer width={this.props.widthTimelineContainer} children={row}>
                </RowTimelinesContainer>
            );
        });
        return (
            <div className="vjs-timeline">
                {RowsTimelines}
            </div>
        );
    }
}

/**
 * div to hold timelines of same type
 */
class RowTimelinesContainer extends Component{
    constructor(props){
        super(props);
    }
    render() {
        let divStyle = {
            top:this.props.top,
            width: this.props.width,
        };
        return(
            <div
                className="rowtimelinescontainer"
                style={divStyle}
                ref={TimelinesContainer => this.TimelinesContainer = TimelinesContainer}>
                {this.props.children}
            </div>
        );
    }
}
/**
 *  timeline element
 */
class SpanTimeline extends Component {
    constructor(props){
        super(props);
        this.handleClick = this.handleClick.bind(this);
        this.handleClickDelTimeline = this.handleClickDelTimeline.bind(this);
        this.state = {
            active:false,
            showButtonDelete:false
        }
    }
    handleClick(e){
        this.props.handleClickSpan(e);
    }
    addActiveClass(e){
        const clicked = e.target.id
        if(this.state.active) {
            this.setState({active: false});
        } else {
            this.setState({active: true})
        }
    }

    /**
     * delete timeline with button
     * @param numtag
     */
    handleClickDelTimeline(idHistory){
        this.props.handleClickDelTimeline(idHistory);   //in TimelinesBlock
    }
    render(){
        let divStyle = {
            width: this.props.width,
            left:this.props.left
        };
        let divDashedStyle = {
            height: this.props.dashedHeight
        }
        return (
            <div className="annotator-hl">
                <div
                    className={`${this.props.class} ${this.props.timelineIsActive? 'active': ''}`}
                    idhistory={this.props.idHistory}
                    id={this.props.id}
                    style={divStyle}
                    start={this.props.start}
                    end={this.props.end}
                    onClick = {this.handleClick}
                    ref={TimelineSpan => this.TimelineSpan = TimelineSpan}
                    title={this.props.titleTL}
                >
                    <TimelineArrow class="c-box--arrow-left"></TimelineArrow>
                    <TimelineArrow class="c-box--arrow-right"></TimelineArrow>
                    <DeleteTimeline
                        showButtonDelete={this.props.timelineIsActive}
                        handleClickDelTimeline={this.handleClickDelTimeline}
                        idHistorySelected={this.props.idHistory}
                    />
                    <DashedDivActiveTimeline style={divDashedStyle} show={this.props.timelineIsActive? true: false}/>
                </div>

            </div>
        );
    }
}
class DeleteTimeline extends Component{
    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }
    handleClick(event) {
        event.stopPropagation()
        this.props.handleClickDelTimeline(this.props.idHistorySelected);
    }
    render(){
        return(
            this.props.showButtonDelete && <div className="btn-del-timeline" onClick={this.handleClick}></div>
        );
    }
}
class DashedDivActiveTimeline extends Component{
    constructor(props){
        super(props);
    }
    render(){
        let classDiv = 'dashed-line';
        return(
            this.props.show && <div className={classDiv} style={this.props.style}></div>
        );
    }
}
class CommentTextarea extends Component{
    constructor(props){
        super(props);
        this.state = {
            value: '',
            isShown: false
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleChange(event) {
        this.setState({value: event.target.value});
    }
    componentWillReceiveProps(nextProps) {
        let val = nextProps.showCommentForm;
        this.setState({isShown: val})

    }
    handleSubmit(event) {
        this.props.handleSaveComment(this.props.idHistorySelected, this.state.value)
        event.preventDefault();
    }
    render() {
        let htmlContent = '';
        if(this.state.isShown) {
            htmlContent = <form onSubmit={this.handleSubmit}>
                <label>
                    <span>Commentaire</span>
                    <textarea value={this.state.value} onChange={this.handleChange} cols={40} rows={5}/>
                </label>
                <input type="submit" value="Valider"/>
            </form>
        }
        //https://codepen.io/austinlyons/pen/ZLEKgN
        return (

            <div className="form-timeline-comment form-inline">
                {htmlContent}
            </div>
        );
    }
}
class TimelineArrow extends Component{
    constructor(props){
        super(props);
        this.state = {
            active:false,
            isDragging: false
        }
    }
    addActiveClass(event){
        const clicked = event.target.id
        if(this.state.active) {
            this.setState({active: false});
        } else {
            this.setState({active: true})
        }
    }
    resizePanel(event){
        if (this.state.isDragging) {
            const delta = event.clientX - this.state.initialPos
            this.setState({
///                delta: 150
            })
        }
    }
    startResize(event, index){
        event.preventDefault();
        this.setState({
            isDragging: true,
            currentPanel: index,
            initialPos: event.clientX
        });
    }
    stopResize(){
        if (this.state.isDragging) {
        }
    }
    render(){
        let arrowStyle = {
            left:this.state.delta
        }
        return(
            <div
                className={`c-box ${this.props.class}`}
                ref={TimelineCursor => this.TimelineCursor = TimelineCursor}
                style={arrowStyle}
                onMouseDown={(e) => this.startResize(e, 1)}
                onMouseUp={() => this.stopResize()}
                onClick={(e)=>e.stopPropagation()}
                onMouseMove={(e) => this.resizePanel(e)}
            >
            </div>
        );
    }
}
class ClearLocalStorage extends Component{
    constructor(props){
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }
    handleClick(){
        localStorage.clear();
    }
    render(){
        return (
            <button onClick={this.handleClick}>Clear storage</button>
        );
    }
}
Number.prototype.between = function(a, b, inclusive) {
    var min = Math.min(a, b),
        max = Math.max(a, b);

    return inclusive ? this >= min && this <= max : this > min && this < max;
}
class ScrollingDiv extends Component{
    constructor(props){
        super(props);
        this.state = {
            timeElapsedSecond : 0
        }
    }
    componentDidMount() {
        this.interval = setInterval(() => this.tick(), 500);
    }
    componentWillUnmount() {
        clearInterval(this.interval);
    }
    tick() {
        this.ScrollingDiv.scrollLeft += 20;
    }
    render(){
        let className='divscrollcontainer';
        return(
            <div className={className} ref={ScrollingDiv => this.ScrollingDiv = ScrollingDiv}>
                <div class="divtoscroll" >
                    fafafafafafa fafzfaz fazf azfa zaf azf azfazfaz azf azfazfazfa zfaz fazf az az az az a za az
                    --------------------------- ______________________________ çççççççççççççççççççççççççççççç dddddddddddddddddddddddddd kkkkkkkkkkkkkkkkkkkkkkkddddddddddd jjjjjjjjjjjjjjjjjjjjjjjjjjjj
                </div>
            </div>
        );
    }
}
import { h, Component } from 'preact'
import videojs from 'video.js'
import {VjsEventsComp} from './VjsEventsComp'

import '!style-loader!css-loader!video.js/dist/video-js.css'

export default class VideoPlayer extends Component {
    constructor(...props) {
        super(...props);
        this.handleReady = this.handleReady.bind(this);
        this.handleRef = this.handleRef.bind(this);

        this.playerId = this.props.idPlayer;
    }

    componentDidMount() {
        this.initPlayer(this.props);
        if (this.player.readyState() < 1) {
            this.player.one("loadedmetadata", ()=>this.onLoadedMetadata());
        }
        else this.onLoadedMetadata();
    }
    onLoadedMetadata(){
        //register plugin
        this.player.TimelinePlugin({handleChangeDivSize: this.props.handleChangeDivSize});
        localStorage.setItem("playerDuration", this.player.duration());
    }
    initPlayer(props) {
        const playerOptions = this.generatePlayerOptions(props);
        this.player = videojs(document.querySelector(`#${this.playerId}`), playerOptions);
        this.player.src(props.src);
        this.player.poster(props.poster);
        this.setControlsVisibility(this.player, props.hideControls);

        let vjsEvents = new VjsEventsComp(props, this.player);
        vjsEvents.initPlayerEvents();
    }

    generatePlayerOptions(props){
        const playerOptions = {};
        playerOptions.controls = props.controls;
        playerOptions.autoplay = props.autoplay;
        playerOptions.preload = props.preload;
        playerOptions.width = props.width;
        playerOptions.height = props.height;
        playerOptions.bigPlayButton = props.bigPlayButton;
        playerOptions.inactivityTimeout = 0;    //always shows controlbar
        const hidePlaybackRates = props.hidePlaybackRates || props.hideControls.includes('playbackrates');
        if (!hidePlaybackRates) playerOptions.playbackRates = props.playbackRates;
        return playerOptions;
    }

    setControlsVisibility(player, hiddencontrols){
        let Controls = {
            "play": "playToggle",
            "volume": "volumePanel",
            "seekbar": "progressControl",
            "timer": "remainingTimeDisplay",
            "playbackrates": "playbackRateMenuButton",
            "fullscreen": "fullscreenToggle"
        };
        Object.keys(Controls).map(c => { player.controlBar[Controls[c]].show() });
        hiddencontrols.map(c => { player.controlBar[Controls[c]].hide() });
    }

    componentWillUnmount() {
        if (this.player) {
            this.player.dispose()
        }
    }

    handleReady() {

    }

    handleRef(node) {
        this.videoNode = node
    }

    // wrap the player in a div with a `data-vjs-player` attribute
    // so videojs won't create additional wrapper in the DOM
    // see https://github.com/videojs/video.js/pull/3856
    /* eslint-disable jsx-a11y/media-has-caption */
    render() {
        return (
            <div>
                <div data-vjs-player>
                    <video
                        className={`video-js ${this.props.className}`}
                        id={this.playerId}
                        ref={this.handleRef}
                        handleTimelineResize={this.props.handleTimelineResize}
                    />
                </div>
            </div>
        )
    }
}

VideoPlayer.defaultProps = {
    src: "",
    poster: "",
    controls: true,
    autoplay: false,
    preload: 'auto',
/*    playbackRates: [0.5, 1, 1.5, 2],*/
    hidePlaybackRates: false,
    className: "",
    hideControls: [],
    bigPlayButton: true,
    bigPlayButtonCentered: true,
    width:720,
    height:420,
    onReady: () => { },
    onPlay: () => { },
    onPause: () => { },
    onTimeUpdate: () => { },
    onSeeking: () => { },
    onSeeked: () => { },
    onEnd: () => { },
};
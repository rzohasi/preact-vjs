export class VjsEventsComp{
    constructor(props, player){
        this.props = props;
        this.player = player;
    }
    initPlayerEvents(){
        let currentTime = 0;
        let previousTime = 0;
        let position = 0;

        this.player.ready(() => {
            this.props.onReady(this.player);
            window.player = this.player;
        });
        this.player.on('play', () => {
            this.props.onPlay(this.player.currentTime());
        });
        this.player.on('pause', () => {
            this.props.onPause(this.player.currentTime());
        });
        this.player.on('timeupdate', (e) => {
            this.props.onTimeUpdate(this.player.currentTime());
            previousTime = currentTime;
            currentTime = this.player.currentTime();
            if (previousTime < currentTime) {
                position = previousTime;
                previousTime = currentTime;
            }
        });
        this.player.on('seeking', () => {
            this.player.off('timeupdate', () => { });
            this.player.one('seeked', () => { });
            this.props.onSeeking(this.player.currentTime());
        });

        this.player.on('seeked', () => {
            let completeTime = Math.floor(this.player.currentTime());
            this.props.onSeeked(position, completeTime);
        });
        this.player.on('ended', () => {
            this.props.onEnd();
        });
    }
}
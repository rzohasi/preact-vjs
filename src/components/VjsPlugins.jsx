import {h, Component, render, createElement} from 'preact'

import videojs from 'video.js';
const Plugin = videojs.getPlugin('plugin');
const videojsComponent = videojs.getComponent('Component');
const videojsButtonClass = videojs.getComponent('Button');
import ControlBoard from "./ControlButton";

/**
 *
 */
class customButtonVjs extends videojsButtonClass{
    constructor(player, options){
        super(player, options);
    }
    handleClick(){
        console.log('click custom button');
    }
    createEl(){
        return videojs.dom.createEl('div', {
            className: 'vjs-custom-button hhhhh',
            id: 'timeline'
        });
    }
}

/**
 *
 * @type {customButtonVjs}
 */
// register plugin
videojs.customButtonVjs = customButtonVjs;
if (videojs.getPlugin('customButtonVjs') === undefined) {
    videojs.registerPlugin('customButtonVjs', customButtonVjs);
}


/**
 * container of custom buttons
 */
class VideojsCustomButtonsContainer extends videojsComponent{
    constructor(player,options){
        super(player, options);

    }
    createEl(){
        return videojs.dom.createEl('div',{
            className: 'vjs-buttons-container hhhj',
        });
    }
}
videojsComponent.registerComponent('VideojsCustomButtonsContainer', VideojsCustomButtonsContainer);
/*
class ButtonTest extends videojsButtonClass{
    constructor(player,options){
        super(player,options);
        this.player = player;
        this.state = {
            ContainerIsShown: false,
        };
    }
    handleClick(){
        if(this.state.ContainerIsShown){
            this.player.getChild('DisplayContainerTest').hide();
        }
        else{
            this.player.getChild('DisplayContainerTest').show();
        }
        this.setState({
            ContainerIsShown: !this.state.ContainerIsShown,
        });
    }
    createEl(){
        return videojs.dom.createEl('button',{
            className: 'vjs-button-test vjs-control vjs-menu-button vjs-button bbbbbbbbbbbbbbbbb',
            title: 'Test',
            innerHTML: 'T',
            role:'button',
            onClick: ()=>this.handleClick(),
        });
    }
}
videojsComponent.registerComponent('ButtonTest', ButtonTest);
*/
/**
 * button for controlbar
 */
class ButtonTimeline extends videojsButtonClass{
    constructor(player,options){
        super(player, options);
        this.player = player;
        this.state = {
            showTimelineContainer: false,
        };
    }
    handleClick(){
        let progressControl = this.player.getChild('ControlBar').getChild('ProgressControl').getChild('SeekBar');
        this.setState({ showTimelineContainer: !this.state.showTimelineContainer });
        if(this.state.showTimelineContainer){
            progressControl.getChild('RSTimeBar').show();
//x            this.player.getChild('VideojsTimelineContainer').show();
            document.getElementById('timelinedisplay').style.display = "";
            this.player.pause();
        }
        else{
            progressControl.getChild('RSTimeBar').hide();
//x            this.player.getChild('VideojsTimelineContainer').hide();
            document.getElementById('timelinedisplay').style.display = "none";
        }
    }
    createEl(){
        return videojs.dom.createEl('button',{
            className: 'vjs-button-timeline-xxx vjs-control vjs-menu-button bbbbbbbbbbbbbbbbb ',
            id:'btn-timeline',
            ref:'btn-timeline',
            title: 'Show timeline',
            innerHTML: 'TL',
            onClick: ()=>this.handleClick(),
        });
    }
}
videojsComponent.registerComponent('ButtonTimeline', ButtonTimeline);


/**
 *
 */
export default class TimelinePlugin extends Plugin {
    constructor(player, options) {
        super(player, options);
        this.options = options;
        this.player = player;
        player.on('playing', function() {
            videojs.log('playback began!');
            document.getElementById('timelinedisplay').style.display = "";
//            this.player.getChild('VideojsTimelineContainer').show();
        });
        this.components = {};
        this.setupUI();
        this.box = this.player.getChild('ControlBar').getChild('ProgressControl').getChild('SeekBar').getChild('RSTimeBar').getChild('SeekRSBar');
        this.idTimelineClicked = 0;
    }
    setValue(index, seconds, writeControlTime){
        let writeControlTime2 = typeof writeControlTime != 'undefined' ? writeControlTime : true;
        let percent = this._percent(seconds);
        let isValidIndex = (index === 0 || index === 1);
        if (isValidIndex){
            this.box.setPosition(index, percent, writeControlTime2);
        }
    }
    _setValuesLocked(start,end){
        this.setValue(0, start);
        this.setValue(1, end);
    }
    _reset(){
        let duration = player.duration();
        let tpr = this.box.getChild('TimePanel').getChild('TimePanelRight').el_;
        let tpl = this.box.getChild('TimePanel').getChild('TimePanelLeft').el_;
        tpl.style.left = '0%';
        tpr.style.left = '100%';
        this._setValuesLocked(0, duration);
    }
    _seconds(percent) {
        ///à verifier let duration = this.player.duration();
        let duration = player.duration();
        if (isNaN(duration)) {
            return 0;
        }
        return Math.min(duration, Math.max(0, percent * duration));
    }
    _percent(seconds) {
        let duration = this.player.duration();
        if (isNaN(duration)) {
            return 0;
        }
        return Math.min(1, Math.max(0, seconds / duration));
    }
    setupUI(){
        let controlBar = this.player.controlBar;
        let seekBar = controlBar.progressControl.seekBar;

        this.player.addChild('VideojsTimelineContainer');
        controlBar.addChild('ButtonTimeline');
        seekBar.addChild('RSTimeBar', {handleChangeDivSize:this.options.handleChangeDivSize});

        this.VideojsTimelineContainer = this.components.VideojsTimelineContainer = this.player.VideojsTimelineContainer;//Background of the panel
        this.TimelineDisplay  = this.player.TimelineDisplay;
    }
}
// register plugin
videojs.TimelinePlugin = TimelinePlugin;
if (videojs.getPlugin('TimelinePlugin') === undefined) {
    videojs.registerPlugin('TimelinePlugin', TimelinePlugin);
}

/**
 *
 */
class VideojsTimelineContainer extends videojsComponent{
    constructor(player, options){
        super(player, options);
        if(!this.options_.hasOwnProperty('showTimelineContainer'))
            this.options_.showTimelineContainer = false;
        this.state = {
            showTimelineContainer:false,
        };
        this.hide()
    }
    createEl(){
        return videojs.dom.createEl('div', {
            className: 'vjs-timeline-container',
            innerHTML: '',
        });
    }
}
videojsComponent.registerComponent('VideojsTimelineContainer', VideojsTimelineContainer);

class RSTimeBar extends videojsComponent{
    constructor(player, options){
        super(player, options);
        videojsComponent.call(this, player, options);
        this.hide();
        this.addChild('SeekRSBar', {player, handleChangeDivSize:options.handleChangeDivSize});
    }
    createEl(){
        return videojs.dom.createEl('div',{
            className: 'vjs-timebar-RS',
            id:'vjstimebarrs'
        });
    }
}
videojsComponent.registerComponent('RSTimeBar', RSTimeBar);

class SeekRSBar extends videojsComponent{
    constructor(player, options){
        super(player, options);
        videojsComponent.call(this, player, options);
        this.player = player;
        this.options = options;

        this.addChild('SelectionBar');
        this.addChild('SelectionBarLeft');
        this.addChild('SelectionBarRight');
        this.addChild('TimePanel');
        this.on('mousedown', this.onMouseDown);
        this.on('touchstart', this.onMouseDown);

        this.rs = videojs.getPlugin('TimelinePlugin');
        this.state = {test:true}

    }
    setPosition(index, left, writeControlTime){
        let _writeControlTime = typeof writeControlTime != 'undefined' ? writeControlTime : true;
        //index = 0 for left side, index = 1 for right side
        // Check for invalid position
        if (isNaN(left)){
            return false;
        }
        // Check index between 0 and 1
        if (!(index === 0 || index === 1)){
            return false;
        }
        // Alias
        let ObjLeft = this.getChild('SelectionBarLeft').el_;
        let ObjRight = this.getChild('SelectionBarRight').el_;
        let Obj = index === 0 ? ObjLeft : ObjRight;
        let tpr = this.getChild('TimePanel').getChild('TimePanelRight').el_;
        let tpl = this.getChild('TimePanel').getChild('TimePanelLeft').el_;
        let bar = this.getChild('SelectionBar');

        if ((index === 0 ? bar.updateLeft(left) : bar.updateRight(left))) {
            Obj.style.left = (left * 100) + '%';
            index === 0 ? bar.updateLeft(left) : bar.updateRight(left);

            this.rs[index === 0 ? 'start' : 'end'] = this.rs.prototype._seconds(left);

            //Fix the problem  when you press the button and the two arrow are underhand
            //left.zIndex = 10 and right.zIndex=20. This is always less in this case:
            if (index === 0) {
                if ((left) >= 0.9)
                    ObjLeft.style.zIndex = 25;
                else
                    ObjLeft.style.zIndex = 10;
            }
            //-- Panel
            let TimeText = videojs.formatTime(this.rs.prototype._seconds(left)),
                tplTextLegth = tpl.children[0].innerHTML.length;
            let MaxP, MinP, MaxDisP;
            if (tplTextLegth <= 4) //0:00
                MaxDisP = this.player_.isFullScreen ? 3.25 : 6.5;
            else if (tplTextLegth <= 5)//00:00
                MaxDisP = this.player_.isFullScreen ? 4 : 8;
            else//0:00:00
                MaxDisP = this.player_.isFullScreen ? 5 : 10;
            if (TimeText.length <= 4) { //0:00
                MaxP = this.player_.isFullScreen ? 97 : 93;
                MinP = this.player_.isFullScreen ? 0.1 : 0.5;
            } else if (TimeText.length <= 5) {//00:00
                MaxP = this.player_.isFullScreen ? 96 : 92;
                MinP = this.player_.isFullScreen ? 0.1 : 0.5;
            } else {//0:00:00
                MaxP = this.player_.isFullScreen ? 95 : 91;
                MinP = this.player_.isFullScreen ? 0.1 : 0.5;
            }
            if (index === 0) {
                tpl.style.left = Math.max(MinP, Math.min(MaxP, (left * 100 - MaxDisP / 2))) + '%';
                if ((tpr.style.left.replace("%", "") - tpl.style.left.replace("%", "")) <= MaxDisP)
                    tpl.style.left = Math.max(MinP, Math.min(MaxP, tpr.style.left.replace("%", "") - MaxDisP)) + '%';
                tpl.children[0].innerHTML = TimeText;
            } else {
                tpr.style.left = Math.max(MinP, Math.min(MaxP, (left * 100 - MaxDisP / 2))) + '%';
                if (((tpr.style.left.replace("%", "") || 100) - tpl.style.left.replace("%", "")) <= MaxDisP)
                    tpr.style.left = Math.max(MinP, Math.min(MaxP, tpl.style.left.replace("%", "") - 0 + MaxDisP)) + '%';
                tpr.children[0].innerHTML = TimeText;
            }
        }
        return true;
    }

    onMouseDown(event){
        event.preventDefault();
        videojs.dom.blockTextSelection();
        videojs.on(document, "mousemove", videojs.bind(this, this.onMouseMove));
        videojs.on(document, "mouseup", videojs.bind(this, this.onMouseUp));
        videojs.on(document, "touchmove", videojs.bind(this, this.onMouseMove));
        videojs.on(document, "touchend", videojs.bind(this, this.onMouseUp));
        this.player.getChild('ControlBar').getChild('ProgressControl').getChild('SeekBar').off("mousedown");//À VERIFIER
    }
    onMouseUp(event){
        videojs.off(document, "mousemove", this.onMouseMove, false);
        videojs.off(document, "mouseup", this.onMouseUp, false);
        videojs.off(document, "touchmove", this.onMouseMove, false);
        videojs.off(document, "touchend", this.onMouseUp, false);
    }
    onMouseMove(event){
        var left = this.calculateDistance(event);
        let SelectionBarLeft_ = this.getChild('SelectionBarLeft');
        let SelectionBarRight_ = this.getChild('SelectionBarRight');
        let side = 0;
        if (SelectionBarLeft_.pressed) {
            this.setPosition(0, left);
        }
        else if (SelectionBarRight_.pressed){
            this.setPosition(1, left);
            side = 1;
        }
        let currentTimePlayer = this.player.currentTime();
        this.updateDivTimeline(side, currentTimePlayer);
    }

    /**
     * update timeline display
     * @param side
     * @param currentTimePlayer
     */
    updateDivTimeline(side, currentTimePlayer){
        this.options.handleChangeDivSize(side, currentTimePlayer); //call handleTimelineResize()
    }
    getRSTBX(){
        return videojs.dom.findPosition(this.el_).left;
    }
    getRSTBWidth(){
        return this.el_.offsetWidth;
    }
    getWidth() {
        return document.getElementById('SelectionBarLeftId').offsetWidth;//does not matter left or right
    };
    calculateDistance(event){
        let rstbX = this.getRSTBX();
        let rstbW = this.getRSTBWidth();
        let handleW = this.getWidth();

        // Adjusted X and Width, so handle doesn't go outside the bar
        rstbX = rstbX + (handleW / 2);
        rstbW = rstbW - handleW;

        // Percent that the click is through the adjusted area
        //during touch event.pageX is not present
        if(event.pageX){
            return Math.max(0, Math.min(1, (event.pageX - rstbX) / rstbW));
        }
        else{
            //this is a touch event
            return Math.max(0, Math.min(1, (event.touches[0].pageX - rstbX) / rstbW));
        }
    }
    createEl(){
        return videojs.dom.createEl('div',{
            className: 'vjs-rangeslider-holder',
        });
    }
}
videojsComponent.registerComponent('SeekRSBar', SeekRSBar);

class SelectionBar extends videojsComponent{
    constructor(player, props){
        super(player, props);
        this.player = player;
//        this.hide();
        this.on('mouseup', this.onMouseUp);
        this.on('touchend', this.onMouseUp);
        this.fired = false;
    }
    onMouseUp(){
        console.log('onMouseUp selectionbar')
    }
    updateLeft(left){
        let SelectionBarRightEl = document.getElementById('SelectionBarRightId');
        let rightVal = SelectionBarRightEl.style.left != '' ? SelectionBarRightEl.style.left : 100;
        let right = parseFloat(rightVal) / 100;
        ///var width = videojs.round((right - left), this.rs.updatePrecision);
        let width = (right - left).toFixed(3); //round necessary for not get 0.6e-7 for example that it's not able for the html css width
        //(right+0.00001) is to fix the precision of the css in html
        if (left <= (right + 0.00001)) {
            this.el_.style.left = (left * 100) + '%';
            this.el_.style.width = (width * 100) + '%';
            return true;
        }
        return false;
    }
    updateRight(right){
        let SelectionBarLeftEl = document.getElementById('SelectionBarLeftId');
        let leftVal = SelectionBarLeftEl.style.left != '' ? SelectionBarLeftEl.style.left : 0;
        let left = parseFloat(leftVal) / 100;
        let width = (right - left).toFixed(3); //round necessary for not get 0.6e-7 for example that it's not able for the html css width
        //(right+0.00001) is to fix the precision of the css in html
        if ((right + 0.00001) >= left) {
            this.el_.style.left = ((right - width) * 100) + '%';
            this.el_.style.width = (width * 100) + '%';
            return true;
        }
        return false;
    }
    createEl(){
        return videojs.dom.createEl('div',{
            className: 'vjs-selectionbar-RS',
        });
    }
}
videojsComponent.registerComponent('SelectionBar', SelectionBar);

class SelectionBarLeft extends videojsComponent{
    constructor(props){
        super(props);
        this.pressed = false;
        this.on('mousedown', this.onMouseDown);
        this.on('touchstart', this.onMouseDown);
    }
    onMouseDown(event){
        event.preventDefault();
        videojs.dom.blockTextSelection();

        this.pressed = true;
        videojs.on(document, "mouseup", videojs.bind(this, this.onMouseUp));
        videojs.on(document, "touchend", videojs.bind(this, this.onMouseUp));
        videojs.dom.addClass(this.el_, 'active');

    }
    onMouseUp(event){
        videojs.off(document, "mouseup", this.onMouseUp, false);
        videojs.off(document, "touchend", this.onMouseUp, false);
        videojs.dom.removeClass(this.el_, 'active');
        this.pressed = false;
    }
    createEl(){
        return videojs.dom.createEl('div',{
            className: 'vjs-rangeslider-handle vjs-selectionbar-left-RS',
            innerHTML: '<div class="vjs-selectionbar-arrow-RS"></div><div class="vjs-selectionbar-line-RS"></div>',
            id:'SelectionBarLeftId'
        });
    }
}
videojsComponent.registerComponent('SelectionBarLeft', SelectionBarLeft);

class SelectionBarRight extends videojsComponent{
    constructor(props){
        super(props);
//        this.hide();
        this.pressed = false;
        this.on('mousedown', this.onMouseDown);
        this.on('touchstart', this.onMouseDown);
    }
    onMouseDown(event){
        event.preventDefault();
        videojs.dom.blockTextSelection();
        this.pressed = true;
        videojs.on(document, "mouseup", videojs.bind(this, this.onMouseUp));
        videojs.on(document, "touchend", videojs.bind(this, this.onMouseUp));
        videojs.dom.addClass(this.el_, 'active');
    }
    onMouseUp(event){
        videojs.off(document, "mouseup", this.onMouseUp, false);
        videojs.off(document, "touchend", this.onMouseUp, false);
        videojs.dom.removeClass(this.el_, 'active');
        this.pressed = false;
    }
    createEl(){
        return videojs.dom.createEl('div',{
            className: 'vjs-rangeslider-handle vjs-selectionbar-right-RS',
            innerHTML: '<div class="vjs-selectionbar-arrow-RS"></div><div class="vjs-selectionbar-line-RS"></div>',
            id:'SelectionBarRightId'
        });
    }
}
videojsComponent.registerComponent('SelectionBarRight', SelectionBarRight);

class TimePanel extends videojsComponent{
    constructor(props){
        super(props);
        this.addChild('TimePanelLeft');
        this.addChild('TimePanelRight');
    }
    createEl(){
        return videojs.dom.createEl('div',{
            className: 'vjs-timepanel-RS'
        });
    }
}
videojsComponent.registerComponent('TimePanel', TimePanel);

class TimePanelLeft extends videojsComponent{
    constructor(props){
        super(props);
    }
    createEl(){
        return videojs.dom.createEl('div', {
            className: 'vjs-timepanel-left-RS',
            innerHTML: '<span class="vjs-time-text">00:00</span>'
        });
    }
}
videojsComponent.registerComponent('TimePanelLeft', TimePanelLeft);

class TimePanelRight extends videojsComponent{
    constructor(props){
        super(props);
    }
    createEl(){
        return videojs.dom.createEl('div', {
            className: 'vjs-timepanel-right-RS',
            innerHTML: '<span class="vjs-time-text">00:00</span>'
        });
    }
}
videojsComponent.registerComponent('TimePanelRight', TimePanelRight);
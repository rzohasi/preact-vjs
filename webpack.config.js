const HtmlWebpackPlugin = require('html-webpack-plugin')
const path = require('path')
const webpack = require('webpack')

const ENV = process.env.NODE_ENV || 'development'

module.exports = {
    mode: 'development',
    entry:'./src/App.jsx',
    output: {
        path: path.join(__dirname, 'dist'),
        filename: "bundle.js"
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                use: 'babel-loader'
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: [
                    'babel-loader',
                ],
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader'],
            },
            {
                test: /\.scss$/,
                use: [
                    "style-loader", // creates style nodes from JS strings
                    "css-loader", // translates CSS into CommonJS
                    "sass-loader" // compiles Sass to CSS, using Node Sass by default
                ]
            },
        ],
    },
    resolve: {
        extensions: ['.jsx', '.js', '.json', '.scss', '.css'],
        modules: [
            path.join(__dirname, 'node_modules'),
        ],
        alias: {
            "react": "preact-compat",
            "react-dom": "preact-compat"
        },
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: 'test node module',
            myPageHeader: 'test create plugin.js',
            template: "./src/index.html",
            filename: "./index.html"
        })
    ],
    devServer: {
        contentBase: path.join(__dirname, 'dist')
    },
    performance: {
        hints: ENV === 'production' ? 'warning' : false
    },
    devtool: "source-map"
}